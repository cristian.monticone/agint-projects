import java.util.Random;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class PizzaCustomerAgent extends Agent {
	private AID choosenVendor;
	private String orderId;

	protected void setup() {
		System.out.println("Pizza customer agent " + getAID().getName()
				+ " ready.");

		// Waiting 2 sec
		try {
			Thread.sleep(1000 + (new Random()).nextInt(30)*100); // from 1000 ms to 4000ms
		}
		catch(InterruptedException ie) {
			ie.printStackTrace();
		}

		DFAgentDescription template = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("pizza-selling");
		template.addServices(sd);

		AID[] availableVendors = null;
		try {
			DFAgentDescription[] result = DFService.search(this, template); 
			System.out.print("[" + this.getLocalName() + "] Found the following pizza vendor agents:");
			availableVendors = new AID[result.length];
			for (int i = 0; i < result.length; ++i) {
				availableVendors[i] = result[i].getName();
				System.out.print(availableVendors[i].getLocalName() + ";");
			}
			System.out.print("\n");
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}

		if ( availableVendors.length > 0 ) {
			// choosing a random vendor. (a smart policy)
			int vIndex = (new Random()).nextInt(availableVendors.length);
			choosenVendor = availableVendors[vIndex];

			this.addBehaviour(new OrderPizzaBehaviour());
		}
		else {
			System.out.println("[" + this.getLocalName() + "] What a shame... No pizza vendors found.");
			doDelete();
		}
	}

	protected void takeDown() {
		System.out.println("Agent " + this.getLocalName() + " has achieved his goal, terminated.");
	}

	private class OrderPizzaBehaviour extends Behaviour {
		private int step = 0;

		private int choosenPizza;
		private MessageTemplate mt;

		private AID deliveryBoy;

		public void action() {
			switch (step) {
			case 0:
				// Choosing a pizza from 0 to 9.
				choosenPizza = (new Random()).nextInt(10);
				step = 1;
				break;
			case 1:
				//order the pizza
				orderId = getName() + "_" + System.currentTimeMillis();
				ACLMessage request = new ACLMessage(ACLMessage.INFORM);
				request.setContent("do-and-deliver-pizza");
				request.setConversationId("order-" + orderId);
				request.addReceiver(choosenVendor);
				myAgent.send(request);
				System.out.println("[" + myAgent.getLocalName()
						+ "] Pizza type " +
						choosenPizza + " order done at " +
						"pizzeria " +
						choosenVendor.getLocalName() +
						".");

				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("order-" + orderId),
				   					MessageTemplate.MatchContent("pizza"));

				myAgent.addBehaviour(new AskForPizzaBehaviour(myAgent, 6000)); // every 6000ms

				step = 2;
				break;
			case 2:
				//waiting for pizza
				ACLMessage pizzaMsg = myAgent.receive(mt);

				if (pizzaMsg != null) {
					System.out.println("[" + myAgent.getLocalName() + "] Received pizza with msg: " + pizzaMsg.getContent());
					deliveryBoy = pizzaMsg.getSender();
					step = 3;
				}
				else {
					block();
				}
				break;
			case 3: 
				//pay the pizza
				ACLMessage payMsg = new ACLMessage(ACLMessage.INFORM);
				payMsg.setContent("pizza-payed");
				payMsg.setConversationId("order-" + orderId);
				payMsg.addReceiver(deliveryBoy);
				myAgent.send(payMsg);
				System.out.println("[" + myAgent.getLocalName() + "] Pizza payed.");

				mt = MessageTemplate.and(MessageTemplate.MatchConversationId("order-" + orderId),
				   				MessageTemplate.MatchSender(deliveryBoy));

				step = 4;
				break;
			case 4:
				//waiting for receipt
				ACLMessage receipt = myAgent.receive(mt);

				if (receipt != null) {
					System.out.println("[" + myAgent.getLocalName() + "] Received receipt with msg: " + receipt.getContent());
					step = 5;
				}
				else {
					block();
				}
				break;
			case 5:
				System.out.println("[" + myAgent.getLocalName() + "] Eating...");
				try {
					Thread.sleep(1000);
				}
				catch(InterruptedException ie) {
					ie.printStackTrace();
				}

				System.out.println("[" + myAgent.getLocalName() + "] The pizza number " +
						choosenPizza + " was so yummy! :-)");
				myAgent.doDelete();
				step = -1;
				break;
			}
		}

		public boolean done() {
			return (step == -1);
		}
	
	}

	private class AskForPizzaBehaviour extends TickerBehaviour {
		MessageTemplate mt;

		public AskForPizzaBehaviour(Agent myAgent, long period) {
			super(myAgent, period);
		} 

		public void onTick() {
			// Ask for pizza.
			System.out.println("[" + myAgent.getLocalName() + "] It's been too long! Asking the pizza vendor if my pizza is ready. :/");
			ACLMessage query = new ACLMessage(ACLMessage.QUERY_IF);
			query.setContent("is-pizza-ready");
			query.setConversationId("order-" + orderId);
			query.setReplyWith("ask-for-pizza" + System.currentTimeMillis());
			query.addReceiver(choosenVendor);
			myAgent.send(query);

			mt = MessageTemplate.and(MessageTemplate.MatchConversationId("order-" + orderId),
					MessageTemplate.MatchInReplyTo(query.getReplyWith()));
			
			myAgent.addBehaviour(new WaitingResponse());
		}	

		public class WaitingResponse extends Behaviour {
			private boolean replied = false;

			public void action() {
				// Wait for response
				ACLMessage reply = myAgent.receive(mt);

				if (reply != null) {
					System.out.println("[" + myAgent.getLocalName() + "] The pizza vendor replied: " + reply.getContent());
					replied = true;
				}
				else {
					block();
				}
			}

			public boolean done() {
				return replied;
			}
		}
	}
}
