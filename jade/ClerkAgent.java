import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashSet;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class ClerkAgent extends Agent {
	private AID chef;
	HashSet<String> activeOrders;

	protected void setup() {
		activeOrders = new HashSet<String>();

		// Register the pizza vendor service in the facilitator service
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		ServiceDescription sd = new ServiceDescription();
		sd.setType("pizza-selling");
		sd.setName("JADE-pizza-vending");
		dfd.addServices(sd);
		try {
			DFService.register(this, dfd);
			System.out.println("Pizza vendor " + getAID().getName() + " registered in DF.");
		}
		catch (FIPAException fe) {
			fe.printStackTrace();
		}

		//get chef name. (easy mode)
		String localName = this.getLocalName();
		Pattern p = Pattern.compile("clerk([0-9]+)");
		Matcher m = p.matcher(localName);
		m.find();
		int pseudoOrganizationId = Integer.parseInt(m.group(1));

		chef = new AID("chef" + pseudoOrganizationId, AID.ISLOCALNAME);

		this.addBehaviour(new ListeningEndOrder());
		this.addBehaviour(new HandleNewOrderBehaviour());
	}

	private class HandleNewOrderBehaviour extends Behaviour {
		MessageTemplate newOrderTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
				MessageTemplate.MatchContent("do-and-deliver-pizza"));

		public void action() {
			ACLMessage request = myAgent.receive(newOrderTemplate); 

			if (request != null) {
				String convId = request.getConversationId();
				AID customer = request.getSender();

				activeOrders.add(convId);
				
				// Delegate to pizza chef
				ACLMessage pizzaRequest = new ACLMessage(ACLMessage.INFORM);
				pizzaRequest.setContent("do-pizza-for-" + customer.getName());
				pizzaRequest.setConversationId(convId);
				pizzaRequest.addReceiver(chef);
				myAgent.send(pizzaRequest);

				myAgent.addBehaviour(new CalmCustomerBehaviour(myAgent, convId, customer));
			}
			else {
				block();
			}
		}

		public boolean done() {
			return false;
		}
	}

	private class CalmCustomerBehaviour extends Behaviour {
		private String orderId;
		private AID customer;
		private MessageTemplate	mt;
		private int step = 0;

		public CalmCustomerBehaviour(Agent myAgent, String orderId, AID customer) {
			super(myAgent);
			this.orderId = orderId;
			this.customer =  customer;

			mt = MessageTemplate.and(MessageTemplate.MatchConversationId(orderId),
					MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.QUERY_IF),
							    MessageTemplate.MatchContent("is-pizza-ready")));

		}

		public void action() {
			// Wait for message
			ACLMessage query = myAgent.receive(mt);

			if (query!=null) {
				String replyTo = query.getReplyWith();

				// calm the customer
				ACLMessage disconfirm = new ACLMessage(ACLMessage.DISCONFIRM);
				disconfirm.setContent("not-yet");
				disconfirm.setConversationId(orderId);
				disconfirm.setInReplyTo(replyTo);
				disconfirm.addReceiver(customer);
				myAgent.send(disconfirm);
			}
			else {
				block();
			}
		}

		public boolean done() {
			return !activeOrders.contains(orderId); // TODO end inform performative
		}
	}

	// Needed for track ended orders.
	public class ListeningEndOrder extends Behaviour {
		public void action() {
			MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
							    MessageTemplate.MatchContent("order-finished"));
			ACLMessage inform = myAgent.receive(mt);

			if (inform != null) {
				// delete from agenda
				activeOrders.remove(inform.getConversationId());
			}
			else {
				block();
			}
		}
	
		public boolean done() {
			return false;
		}
	}
}
