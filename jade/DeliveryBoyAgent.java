import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class DeliveryBoyAgent extends Agent {
	private AID chef;
	private AID clerk;

	protected void setup() {
		//get clerk, chef AID. (easy way)
		String localName = this.getLocalName();
		Pattern p = Pattern.compile("delivery([0-9]+)");
		Matcher m = p.matcher(localName);
		m.find();
		int pseudoOrganizationId = Integer.parseInt(m.group(1));
		chef = new AID("chef" + pseudoOrganizationId, AID.ISLOCALNAME);
		clerk = new AID("clerk" + pseudoOrganizationId, AID.ISLOCALNAME);

		this.addBehaviour(new DeliveryPizzaBehaviour());
	}

	private class DeliveryPizzaBehaviour extends Behaviour {
		private int step = 0;
		private ACLMessage request; //the current one
		private AID customer; //the current one

		public void action() {
			switch(step) {
			case 0:
				// Wait for pizza to deliver
				MessageTemplate mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
							MessageTemplate.MatchSender(chef));
				request = myAgent.receive(mt);

				if (request != null) {
					step = 1;
				}
				else {
					block();
				}
				break;
			case 1:
				customer = new AID(request.getContent().split("deliver-to-")[1], AID.ISGUID);

				// "deliver" the pizza
				ACLMessage delivery = new ACLMessage(ACLMessage.INFORM);
				delivery.setContent("pizza");
				delivery.setConversationId(request.getConversationId());
				delivery.addReceiver(customer);
				myAgent.send(delivery);

				step = 2;
				break;
			case 2:
				// Waiting the payment
				mt = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM), 
							MessageTemplate.MatchConversationId(request.getConversationId()));
				ACLMessage payment = myAgent.receive(mt);

				if (payment != null) {
					step = 3;
				}
				else {
					block();
				}
				break;
			case 3:
				// sending receipt
				ACLMessage receipt = new ACLMessage(ACLMessage.INFORM);
				receipt.setContent("receipt");
				receipt.setConversationId(request.getConversationId());
				receipt.addReceiver(customer);
				myAgent.send(receipt);

				//inform clerk that this order is finished.
				ACLMessage inform = new ACLMessage(ACLMessage.INFORM);
				inform.setContent("order-finished");
				inform.setConversationId(request.getConversationId());
				inform.addReceiver(clerk);
				myAgent.send(inform);

				step = 0; // I need to serve other customers now!
				break;
			}
		}

		public boolean done() {
			return false;
		}
	}

}
