import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Random;

import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.*;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;

public class PizzaChefAgent extends Agent {
	private AID deliveryBoy;

	protected void setup() {
		//get delivery boy AID. (easy mode)
		String localName = this.getLocalName();
		Pattern p = Pattern.compile("chef([0-9]+)");
		Matcher m = p.matcher(localName);
		m.find();
		int pseudoOrganizationId = Integer.parseInt(m.group(1));

		deliveryBoy = new AID("delivery" + pseudoOrganizationId, AID.ISLOCALNAME);

		this.addBehaviour(new DoPizzaBehaviour());
	}

	private class DoPizzaBehaviour extends Behaviour {
		private MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);

		public void action() {
			// Wait for pizza request
			ACLMessage request = myAgent.receive(mt);
			if (request != null) {
				// "doing" the pizza.
				try {
					Thread.sleep((new Random()).nextInt(23)*1000 + 2000); // From 2 to 25 seconds.
				}
				catch(InterruptedException ie) {
					ie.printStackTrace();
				}

				AID customer = new AID(request.getContent().split("do-pizza-for-")[1], AID.ISGUID);

				// To delivery boy
				ACLMessage delivery = new ACLMessage(ACLMessage.INFORM);
				delivery.setContent("deliver-to-" + customer.getName());
				delivery.setConversationId(request.getConversationId());
				delivery.addReceiver(deliveryBoy);
				myAgent.send(delivery);
			}
			else {
				block();
			}
		}

		public boolean done() {
			return false;
		}
	}

}
