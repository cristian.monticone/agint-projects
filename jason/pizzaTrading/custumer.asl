/* Initial beliefs and rules */

hungry_for_pizza.

/* Initial goals */

/* Plans */

// Quando sono affamato di pizza ne ordino una.
+hungry_for_pizza : not pizza_available & not ordering_pizza <-
	+ordering_pizza;
	!search_pizzeria;
	!order_pizza.

// Trova un clerk di una pizzeria con politica casuale
+!search_pizzeria <-
	.wait(1000);
	.df_search("make_order(pizza)", Pizzerie);
	.shuffle(Pizzerie, [Choosen | _]); 
	.print("Scelta pizzeria: ", Choosen);
	+clerk(Choosen).

+!order_pizza <-
	!select_pizza(P);
	!make_an_order(P).

+!select_pizza(P) <-
	P = math.floor(math.random(10)).

+!make_an_order(P) : clerk(Clerk) <- 
	.send(Clerk, achieve, make_order(P));
	!wait_delivery_boy ||| !wait_then_ask.

+!wait_then_ask <-
	.wait(1000); // ogni 1000 msec
	!ask.
+!ask : not pizza_received & clerk(Clerk) <-
	.print("Whre is my pizza!? asking the clerk.");
	.send(Clerk, achieve, tell_pizza_ready);
	!wait_then_ask.
+!ask.

+pizza_not_yet_ready[source(Other)] : clerk(Other) <-
	.print("Clerk replied: pizza not yet ready.");
	-pizza_not_yet_ready[source(Other)].

// Aspettiamo la pizza.
+!wait_delivery_boy <-
	.wait({+pizza_delivered});
	+pizza_received;
	+pizza_available;
	.print("pizza arrivata");
	!pay_the_pizza;
	!wait_receipt;
	!eat_the_pizza.

+!pay_the_pizza : pizza_delivered[source(DelBoy)] <-
	.send(DelBoy, tell, pizza_payed);
	.print("pizza pagata").

+!wait_receipt <-
	.wait({+receipt_received[source(DelBoy)]});
	.print("Fattura ricevuta.").

+!eat_the_pizza : pizza_available <-
	.print("Mangiando...");
	-hungry_for_pizza;
	-pizza_available;
	.print("La pizza era molto buona.").
