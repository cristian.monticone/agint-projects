/* Initial beliefs and rules */

/* Initial goals */
!init_org.

/* Plans */
+!init_org <-
	.my_name(Me);
	.substring(Me, Org, 12);
	.concat("clerk", Org, Clerk);
	+clerk(Clerk).

+!deliver_pizza_to(Custumer)[source(Other)] : clerk(Clerk) <-
	.send(Custumer, tell, pizza_delivered);
	.wait(pizza_payed[source(Custumer)]);
	.send(Custumer, tell, receipt_received);
	.send(Clerk, untell, order_pending(Customer)).
