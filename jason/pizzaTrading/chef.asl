/* Initial beliefs and rules */

/* Initial goals */
!init_org.

/* Plans */
+!init_org <-
	.my_name(Me);
	.substring(Me, Org, 4);
	.concat("delivery_boy", Org, DelBoy);
	+delivery_boy(DelBoy);
	!handle_orders.

+!handle_orders : delivery_boy(DeliveryBoy) <-
	.wait(pending_order_to(Customer)); // l'argomento non è un trigger bensi una logical expression!
	.wait(3000); // "doing" the pizza
	.send(DeliveryBoy, achieve, deliver_pizza_to(Customer));
	-pending_order_to(Customer);
	!handle_orders.

+!do_pizza_for(Customer)[source(Other)] <-
	+pending_order_to(Customer).
