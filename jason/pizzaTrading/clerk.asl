/* Initial beliefs and rules */

/* Initial goals */
!init_org.
!register_organization.

/* Plans */
+!init_org <-
	.my_name(Me);
	.substring(Me, Org, 5);
	.concat("chef", Org, Chef);
	+chef(Chef).

+!register_organization <-
	.df_register("make_order(pizza)").

+!make_order(P)[source(Other)] : chef(Chef) <-
	+pending_order(Other);
	.send(Chef, achieve, do_pizza_for(Other)).

+!tell_pizza_ready[source(Other)] : pending_order(Other) <-
	.send(Other, tell, pizza_not_yet_ready).
